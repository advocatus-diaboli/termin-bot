import asyncio
from logging import warning
from re import escape
from attr import __description__
import discord
from discord import client
from discord.ext import commands

import io
import os
from time import sleep, time

import discordToken

from postDialogue import PostDialogue
from entryDialogue import EntryDialogue
from editDialogue import EditDialogue
from announceDialogue import AnnounceDialogue
from messageDialogue import MessageDialogue
from kickDialogue import KickDialogue

import botUtil
import fileSave

import dateTimeConv
import datetime

from eventLink import EventLink

from log import Log

from botExceptions import *

class WieselBot(discord.Client):

    dialogues = {}

    posts = {}
    adminPosts = {}

    eventLink = None

    guildInfos = {}

    sweepRunning = False

    HELP = \
'''Ich bin das Termin Wiesel :smile:
Ich bin dafür da die Rundenaushänge und Terminfindung zu organisieren und das ganze etwas übersichtlicher zu gestalten. Vielleicht kennst du von anderen Servern den 'Chaos Bot' oder den 'Sesh Bot'. So ähnlich mache ich das auch. Wenn du ganz viele Termine abfragen willst (mehr als 21), dann solltest du totzdem ein Doodle, Nuudel o.Ä. erstellen. Ansonsten kann ich mich ab jetzt darum kümmern. :wink:

Wenn du eine neue Runde (oder ein vergleichbares Event) aushängen willst, dann kannst du hier, in diesem Channel einfach

> **!post**

schreiben. Ich frag dich dann alles wichtige ab.

Du musst am Ende dem Post eine Kategorie zuweisen:

**Terminfindung:**   Du stellst mehrere mögliche Termine zur Auswahl.

**Spielendensuche:** Der/Die Termine stehen fest, es wird nach Spielenden gesucht, die können.

**Kalendereintrag**: Termin(e) und Spielende stehen fest. Trägt sich bei Kalendereinträgen noch jmd. ein oder aus, wirst du benachrichtigt.

Nach der Erstellung eines Posts kriegst du von mir eine **Admin-Nachricht**. Mit den Knöpfen darunter kannst du deinen Aushang *löschen*, *bearbeiten* oder den *Termin festlegen* bzw. den Termin in den *Kalender eintragen*.

Da ich im Privatchat mit dir keine Berechtigungen habe, kann ich den Chatverlauf nicht so übersichtlich gestalten, wie hier. Pass also auf, dass du nicht den Überblick verlierst, wenn du viel bearbeitest oder viele Runden ausgehangen hast.

Wenn du den DM Channel doch mal aufräumen willst, kannst du '!clear' schreiben. Ich lösche dann Nachrichten von mir, die nicht mehr gebraucht werden. Das kann ein paar Minuten dauern.

PS: Diesen Channel halte ich sauber. Das bedeutet, dass ich alles, was hier drin landet wieder entferne.'''

    TWHELP = \
'''Moin :smile:
Es gibt nur ein paar wichtige Befehle, der Rest ist selbsterklärend oder wird erläuert:

> **!cmdChannel**
> **!dateChannel**
> **!playerChannel**
> **!calendarChannel**
> **!imgCache**

sind dazu da, um festzulegen, in welchen Chat ich was für Nachrichten schicke. Wenn diese Befehle noch nie ausgeführt wurden, dann antworte/poste ich immer in dem Chat, in dem ich angeschrieben wurde. Wenn kein Channel als imgCache festgelegt wurde, dann kann man keine Bilder als Anhang hochladen, die speichere ich nämlich im imgCache zwischen.

Der wichtigste Befehl ist dabei:

> **!cmdChannel**

Der Befehl legt fest, wo ich auf den Kernbefehl

> **!post**

reagiere. In diesen Channel schicke ich dann eine ausführliche Erklärung und alles andere, was da rein geschickt wird und kein **!post** ist lösche ich sofort. Sobald der **!cmdChannel** festgelegt wurde, sollten auch die anderen Kanäle festgelegt werden.

Sollte jemand beim posten einschlafen gibt es:

> **!kill**

Der Befehl beendet den derzeit laufenden Dialog, davon kann nämlich immer nur einer zur Zeit laufen. Aber aufpassen, **!kill** kann natürlich gut zum Trollen verwendet werden.

Letztenendes gibt es noch 

> **!clear**

der löscht alle Nicht-Post Nachrichten von mir, die ich in den letzten ~100 Nachrichten finde.
'''

    async def getUser(self, id):
        try:
            return await self.fetch_user(id)
        except discord.NotFound:
            return None
    
    async def getChannel(self, id):
        try:
            return await self.fetch_channel(id)
        except discord.NotFound:
            return None
    
    async def getGuild(self, guildId):
        try:
            return await self.fetch_guild(guildId)
        except discord.NotFound:
            return None
    
    async def getMessage(self, channelId, msgId):
        try:
            ch = await self.getChannel(channelId)
            if ch is None:
                return None
            return await ch.fetch_message(msgId)
        except discord.NotFound:
            return None

    async def getCmdChannel(self, guildId):
        if not guildId in self.guildInfos or not "cmdChannel" in self.guildInfos[guildId]:
            return None
        return await self.getChannel(self.guildInfos[guildId]["cmdChannel"])

    async def getDateChannel(self, guildId):
        if not guildId in self.guildInfos or not "dateChannel" in self.guildInfos[guildId]:
            return None
        return await self.getChannel(self.guildInfos[guildId]["dateChannel"])

    async def getPlayerChannel(self, guildId):
        if not guildId in self.guildInfos or not "playerChannel" in self.guildInfos[guildId]:
            return None
        return await self.getChannel(self.guildInfos[guildId]["playerChannel"])
    
    async def getCalendarChannel(self, guildId):
        if not guildId in self.guildInfos or not "calendarChannel" in self.guildInfos[guildId]:
            return None
        return await self.getChannel(self.guildInfos[guildId]["calendarChannel"])

    async def getImgCache(self, guildId):
        if not guildId in self.guildInfos or not "imgCache" in self.guildInfos[guildId]:
            return None
        return await self.getChannel(self.guildInfos[guildId]["imgCache"])

    async def removeOldPosts(self):
        try:
            remKeys = []

            for postKey, data in self.posts.items():
                if await self.getMessage(*data["post"]) is None:
                    remKeys.append(postKey)
            
            for key in remKeys:
                self.posts.pop(key)
            
            fileSave.save(self.posts, "data/Posts.json")

            remKeys = []
            for pid, aPost in self.adminPosts.items():
                if not aPost["post"][1] in self.posts:
                    remKeys.append(pid)
            
            for key in remKeys:
                self.adminPosts.pop(key)
            
            fileSave.save(self.adminPosts, "data/AdminPosts.json")
        except:
            Log.exception("Failed to remove old Posts")

    def addDialogue(self, id, dlg):
        now = time()

        self.dialogues[id] = (now, dlg)
    
    async def closeDialogue(self, id):
        if id in self.dialogues:
            await self.dialogues[id][1].close()
    
    async def filterDialogues(self, olderThanMinutes = 60):
        remove = []
        now = time()
        for id, tup in self.dialogues.items():
            if now - tup[0] > olderThanMinutes * 30:
                remove.append(id)
        
        for id in remove:
            await self.closeDialogue(id)
        
        return len(remove)

    async def on_ready(self):
        act = discord.Game("Wiesel (!post)")
        await self.change_presence(activity = act)

        Log.status('Logged in: ')
        Log.status('Username: ' + self.user.name)
        Log.status('Id:       ' + str(self.user.id))
        Log.status('Status:   ' + str(act))
        Log.status('------------------------')
        Log.emptyLine()

        self.eventLink = EventLink()

        self.posts = fileSave.load("data/Posts.json")
        self.adminPosts = fileSave.load("data/AdminPosts.json")
        self.guildInfos = fileSave.load("data/GuildInfos.json")
        
        await self.removeOldPosts()

        if not self.sweepRunning:

            self.sweepRunning = True

            while self.sweepRunning:
                try:

                    deleted = 0

                    #Log.status("Starting auto-delete sweep...")

                    for msgId, infoDict in self.posts.items():
                        if "autodel" in infoDict:
                            if float(infoDict["autodel"]) < time():
                                msg = await self.getMessage(*infoDict["post"])
                                await botUtil.tryDelete(msg)

                                Log.status("Deleting post '", infoDict["name"], "'")
                            
                                await botUtil.sendNotification(
                                    await self.getUser(infoDict["user"]),
                                    "Auto-Delete: *" + infoDict["name"] + "*",
                                    "Der Post '*" + infoDict["name"] + "*' wurde gelöscht."
                                )

                                deleted += 1

                        elif len(infoDict["dates"]) > 0:
                            if (float(infoDict["dates"][-1]) < time() if infoDict["type"] > 0 else float(sorted(infoDict["dates"].keys())[-1]) < time()):
                                await self.updatePost(msgId)
                                
                                Log.status("Marked post '", infoDict["name"], "' for auto-delete in 72h")

                    
                    stts = "\tFinished auto-delete sweep:"

                    deletedTotal = deleted

                    if deleted:
                        await self.removeOldPosts()
                    
                        stts += " Deleted " + str(deleted) + " posts."

                    deleted = self.eventLink.filterEvents()
                    deletedTotal += deleted
                    
                    if deleted:
                        stts += " Cleared " + str(deleted) + " old events."

                    deleted = await self.filterDialogues()
                    deletedTotal += deleted
                    
                    if deleted:
                        stts += " Cleared " + str(deleted) + " old dialogues."

                    if deletedTotal:
                        Log.status(stts)

                    #Log.emptyLine()
                
                except:
                    Log.exception("Auto-Delete sweep failed!")

                await asyncio.sleep(901)
    
    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if message.content.lower() == "!twhelp":
            await message.channel.send(
                None,
                embed=discord.Embed(
                    title="Termin Wiesel Befehlhilfe:",
                    description=self.TWHELP,
                    color=discord.Color(0xfd0061)
                ) \
                .set_thumbnail(url = self.user.avatar.url if self.user.avatar is not None else None) \
                .set_author(name = self.user.name)
            )



        if type(message.channel) is discord.TextChannel and message.content.lower() == "!kill" and message.channel.id in self.dialogues and message.channel.permissions_for(message.author).manage_messages:
            await self.closeDialogue(message.channel.id)
            await botUtil.tryDelete(message)
            return
            
        if message.channel.id in self.eventLink:
            await self.eventLink.raiseEvent(message.channel.id, message)
            return
        elif message.guild is None:
            if message.content == "!clear":
                await self.removeOldPosts()

                def shouldDeleteMsg(m):
                    return m.author == self.user and not m.id in self.posts and not m.id in self.adminPosts
                counter = 0

                async for message in message.channel.history(limit=100):
                    if shouldDeleteMsg(message):
                        await message.delete()
                        counter += 1
                
                tmp = await message.channel.send('Deleted {} message(s)'.format(counter))
                await tmp.add_reaction("🔥")
            return

        msg = message.content.strip()

        cmdChannel = await self.getCmdChannel(message.guild.id)
        shouldDelete = not cmdChannel is None and cmdChannel == message.channel

        if cmdChannel is None:
            cmdChannel = message.channel
        
        if len(msg) <= 1 or msg[0] != '!':
            if shouldDelete:
                await botUtil.tryDelete(message)
            return
        
        msg = msg[1:].split(' ', 1)

        msg[0] = msg[0].lower()

        cmdChannel = await self.getCmdChannel(message.guild.id)
        if cmdChannel is None:
            cmdChannel = message.channel

        gId = message.guild.id

        if msg[0] == 'post':
            if message.channel != cmdChannel:
                return

            postName = None
            if len(msg) == 2:
                postName = msg[1]
            msg = await cmdChannel.send(message.author.mention + " *erstellt gerade eine Runde...*")
            await message.delete()
            await self.createPostDialogue(user = message.author, channel = cmdChannel, tmpMsg = msg, name = postName, imgCache = await self.getImgCache(message.guild.id))
        elif msg[0] == 'cmdchannel':
            if not gId in self.guildInfos:
                self.guildInfos[gId] = {}
            self.guildInfos[gId]["cmdChannel"] = message.channel.id
                        
            await botUtil.tryDelete(message)
            fileSave.save(self.guildInfos, "data/GuildInfos.json")

            await message.channel.send(
                None,
                embed=discord.Embed(
                    title="Moin :wave:",
                    description=self.HELP,
                    color=discord.Color(0xfd0061)
                ) \
                .set_thumbnail(url = self.user.avatar.url if self.user.avatar is not None else None) \
                .set_author(name = self.user.name)
            )

            Log.status("Set cmdChannel to " + str(message.channel) + " for Guild " + str(message.guild))

        elif msg[0] == 'datechannel':
            if not gId in self.guildInfos:
                self.guildInfos[gId] = {}
            self.guildInfos[gId]["dateChannel"] = message.channel.id
            
            await botUtil.tryDelete(message)
            fileSave.save(self.guildInfos, "data/GuildInfos.json")
            
            Log.status("Set dateChannel to " + str(message.channel) + " for Guild " + str(message.guild))

        elif msg[0] == 'playerchannel':
            if not gId in self.guildInfos:
                self.guildInfos[gId] = {}
            self.guildInfos[gId]["playerChannel"] = message.channel.id
            
            await botUtil.tryDelete(message)
            fileSave.save(self.guildInfos, "data/GuildInfos.json")
            
            Log.status("Set playerChannel to " + str(message.channel) + " for Guild " + str(message.guild))

        elif msg[0] == 'calendarchannel':
            if not gId in self.guildInfos:
                self.guildInfos[gId] = {}
            self.guildInfos[gId]["calendarChannel"] = message.channel.id
            
            await botUtil.tryDelete(message)
            fileSave.save(self.guildInfos, "data/GuildInfos.json")
        elif msg[0] == 'imgcache':
            if not gId in self.guildInfos:
                self.guildInfos[gId] = {}
            self.guildInfos[gId]["imgCache"] = message.channel.id
            
            await botUtil.tryDelete(message)
            fileSave.save(self.guildInfos, "data/GuildInfos.json")
        elif msg[0] == 'clear':
            await self.removeOldPosts()
            
            def shouldDeleteMsg(m):
                return m.author == self.user and not m.id in self.posts and not m.id in self.adminPosts
            
            deleted = await message.channel.purge(limit=100, check=shouldDeleteMsg)
            tmp = await message.channel.send('Deleted {} message(s)'.format(len(deleted)))
            await tmp.add_reaction("🔥")
            
        else:
            if shouldDelete and message.channel == cmdChannel:
                await botUtil.tryDelete(message)

    
    async def on_raw_reaction_add(self, payload):
        if payload.user_id == self.user.id:
            return
        
        if not payload.message_id in self.eventLink:
            await self.onReaction(True, str(payload.emoji), await self.getMessage(payload.channel_id, payload.message_id), await self.getUser(payload.user_id))
        else:
            await self.eventLink.raiseEvent(payload.message_id, True, str(payload.emoji), await self.getMessage(payload.channel_id, payload.message_id), await self.getUser(payload.user_id))

    async def on_raw_reaction_remove(self, payload):
        if payload.user_id == self.user.id:
            msg = await self.getMessage(payload.channel_id, payload.message_id)
            if not msg is None:
                await msg.add_reaction(str(payload.emoji))
            return
        
        if not payload.message_id in self.eventLink:
            await self.onReaction(False, str(payload.emoji), await self.getMessage(payload.channel_id, payload.message_id), await self.getUser(payload.user_id))
        else:
            await self.eventLink.raiseEvent(payload.message_id, False, str(payload.emoji), await self.getMessage(payload.channel_id, payload.message_id), await self.getUser(payload.user_id))

    async def onReaction(self, wasSet, emoji, msg, user):
        if msg is None:
            return
        
        try:
            if wasSet and msg.id in self.posts:
                if emoji == "📝" and not self.posts[msg.id]["type"]:
                    if user.dm_channel is None:
                        await user.create_dm()
                    
                    if user.dm_channel.id in self.dialogues:
                        await self.closeDialogue(user.dm_channel.id)

                    dlg = EntryDialogue(self.eventLink, user, user.dm_channel, self.posts[msg.id].copy(), msg.id)
                    self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)
                    self.addDialogue(user.dm_channel.id, dlg)

                    await msg.remove_reaction(emoji, user)
                    await dlg.start()
                
                elif emoji == "✅" and self.posts[msg.id]["type"]:
                    
                    await msg.remove_reaction(emoji, user)

                    Log.status(botUtil.fullName(user), " checked in for '", self.posts[msg.id]["name"], "'")

                    if not user.id in self.posts[msg.id]["entries"]:
                        self.posts[msg.id]["entries"].append(user.id)

                        try:
                            await self.updatePost(msg.id)
                        except PostUpdateFailedException:
                            Log.error("Failed to update Post after entry")
                            await botUtil.sendNotification(user, "Ein Fehler ist aufgetreten!", "Beim Bearbeiten des alten Posts ist es leider zu einem Fehler gekommen.")
                            return

                        if self.posts[msg.id]["type"] == 2:
                            author = await self.getUser(self.posts[msg.id]["user"])
                            await botUtil.sendNotification(
                                recp=author,
                                title="Neue:r Spielende:r",
                                message=user.mention + " hat sich für die Runde *" + self.posts[msg.id]["name"] + "* eingetragen."
                            )

                elif emoji == "❌" and self.posts[msg.id]["type"]:
                    Log.status(botUtil.fullName(user), " checked out for '", self.posts[msg.id]["name"], "'")
                    
                    await msg.remove_reaction(emoji, user)

                    if user.id in self.posts[msg.id]["entries"]:
                        pos = self.posts[msg.id]["entries"].index(user.id)
                        self.posts[msg.id]["entries"].pop(pos)

                        lim = self.posts[msg.id]["limit"]

                        try:
                            await self.updatePost(msg.id)
                        except PostUpdateFailedException:
                            Log.error("Failed to update Post after entry")
                            await botUtil.sendNotification(user, "Ein Fehler ist aufgetreten!", "Beim Bearbeiten des alten Posts ist es leider zu einem Fehler gekommen.")
                            return
                        
                        fill = None

                        if lim is not None and pos < lim and len(self.posts[msg.id]["entries"]) >= lim:
                            recp = await self.getUser(self.posts[msg.id]["entries"][lim - 1])

                            fill = recp

                            await botUtil.sendNotification(recp, "*" + self.posts[msg.id]["name"] + "*: Du bist nachgerückt!", "Es hat jemand seinen Platz in der Runde '*" + self.posts[msg.id]["name"] + "*' aufgegeben. Du bist nachgerückt. Wenn du doch nicht mehr kannst, kannst du dich wieder austragen.\n\nHier findest du den Post: " + msg.jump_url)

                        if self.posts[msg.id]["type"] == 2:
                            author = await self.getUser(self.posts[msg.id]["user"])
                            await botUtil.sendNotification(
                                recp=author,
                                title="Spieler:in abgesagt",
                                message=user.mention + " hat sich für die Runde *" + self.posts[msg.id]["name"] + "* ausgetragen." +
                                    ("" if fill is None else "\n" + fill.mention + " ist nachgerückt.")
                            )
                
                elif emoji == "🔄":
                    await self.updatePost(msg.id)
                    await msg.remove_reaction(emoji, user)
                
            elif wasSet and msg.id in self.adminPosts:
                if emoji == "✏️":
                    #await msg.remove_reaction(emoji, user) # Will always throw, because the message is in a DM Channel
                    if msg.channel.id in self.dialogues:
                        await self.closeDialogue(msg.channel.id)
                    
                    postId = self.adminPosts[msg.id]["post"][1]

                    dlg = EditDialogue(self.eventLink, user, msg.channel, self.posts[postId], msg.id, await self.getImgCache(self.adminPosts[msg.id]["guild"]))

                    self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)

                    self.addDialogue(msg.channel.id, dlg)

                    await dlg.start()
                elif emoji == "👥":
                    if msg.channel.id in self.dialogues:
                        await self.closeDialogue(msg.channel.id)
                    
                    postId = self.adminPosts[msg.id]["post"][1]

                    dlg = KickDialogue(self.eventLink, user, msg.channel, self.posts[postId].copy())
                    
                    self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)

                    self.addDialogue(msg.channel.id, dlg)

                    await dlg.start()

                elif emoji == "📻":
                    if msg.channel.id in self.dialogues:
                        await self.closeDialogue(msg.channel.id)
                    
                    postId = self.adminPosts[msg.id]["post"][1]

                    dlg = MessageDialogue(self.eventLink, user, msg.channel, self.posts[postId])

                    self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)

                    self.addDialogue(msg.channel.id, dlg)

                    await dlg.start()
                elif emoji == "🗑️":
                    postMsg = await self.getMessage(*self.adminPosts[msg.id]["post"])
                    await botUtil.tryDelete(postMsg, msg)

                    postName = self.adminPosts[msg.id]["name"]

                    self.posts.pop(postMsg.id)
                    self.adminPosts.pop(msg.id)

                    fileSave.save(self.posts, fileName="data/Posts.json")
                    fileSave.save(self.adminPosts, fileName="data/AdminPosts.json")
                    
                    Log.status("Deleted post for '", postName, "'")
                    Log.emptyLine()
                    
                elif emoji == "📯":
                    if msg.channel.id in self.dialogues:
                        await self.closeDialogue(msg.channel.id)
                    
                    postId = self.adminPosts[msg.id]["post"][1]

                    if self.posts[postId]["type"] == 2:
                        return
                    elif self.posts[postId]["type"] == 1:
                        self.adminPosts[msg.id]["type"] = 2
                        self.posts[postId]["type"] = 2
                        
                        guild = await self.getGuild(self.posts[postId]["guild"])
                    
                        channel = await self.getCalendarChannel(guild.id)
                        
                        if channel is None:
                            channel = msg.channel
                        
                        try:
                            await self.createPost(self.posts[postId].copy(), channel, user)
                        except PostCreationFailedException:
                            Log.error("Failed to announce a Post! Aborting.")
                            await botUtil.sendNotification(user, "Ein Fehler ist aufgetreten!", "Beim Erstellen des Kalendereintrages ist es leider zu einem Fehler gekommen.")
                            return

                        postMsg = await self.getMessage(*self.adminPosts[msg.id]["post"])
                        await botUtil.tryDelete(postMsg, msg)

                        self.posts.pop(postMsg.id)
                        self.adminPosts.pop(msg.id)
                        
                        fileSave.save(self.posts, fileName="data/Posts.json")
                        fileSave.save(self.adminPosts, fileName="data/AdminPosts.json")
        
                        return
                    
                    dlg = AnnounceDialogue(self.eventLink, user, msg.channel, self.posts[postId], msg.id)

                    self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)

                    self.addDialogue(msg.channel.id, dlg)

                    await dlg.start()
            elif wasSet and emoji == "🔥" and not (msg.id in self.posts or msg.id in self.adminPosts):
                if msg.author == self.user:
                    await msg.delete()
            #elif wasSet:
            #    Log.debug("Ignored reaction (wasSet = ", wasSet, ") for reaction = ", emoji, " from User ", user.name, '#', user.discriminator, " on msg ", msg.id)
        except Exception:
            Log.exception("Exception occured when handling reaction")

    async def createPostDialogue(self, user, channel, tmpMsg, name = None, imgCache = None):
        if channel.id in self.dialogues:
            await self.closeDialogue(channel.id)
        
        dlg = PostDialogue(self.eventLink, user, channel, tmpMsg, name, imgCache)

        self.eventLink.registerEvent(dlg.getId(), self.onDialogueClose)

        self.addDialogue(channel.id, dlg)

        await dlg.start()
    
    async def onDialogueClose(self, channelId):
        if not channelId in self.dialogues:
            Log.warning("This dialogue is not known to me.")
            return
        
        try:

            dlg = self.dialogues[channelId][1]

            self.eventLink.unregisterEvent(dlg.getId())

            if type(dlg) is PostDialogue:
                await botUtil.tryDelete(dlg.tmpMsg)
                if dlg.isDone():

                    infoDict = dlg.getDict()
                    
                    channel = await self.getChannelForPost(infoDict, dlg.channel.guild)

                    if channel is None:
                        channel = dlg.channel
                    
                    try:
                        await self.createPost(infoDict, channel, dlg.user)
                    except PostCreationFailedException:
                        Log.error("Failed to create new Post! Aborting.")
                        await botUtil.sendNotification(dlg.user, "Ein Fehler ist aufgetreten!", "Beim Erstellen des Posts ist es leider zu einem Fehler gekommen.")
                        return

            elif type(dlg) is EntryDialogue:
                if dlg.isDone():
                    for date, value in dlg.dates.items():
                        if value:
                            if not dlg.user.id in self.posts[dlg.msgId]["dates"][date]:
                                self.posts[dlg.msgId]["dates"][date].append(dlg.user.id)
                        else:
                            if dlg.user.id in self.posts[dlg.msgId]["dates"][date]:
                                self.posts[dlg.msgId]["dates"][date].remove(dlg.user.id)
                    
                    try:
                        await self.updatePost(dlg.msgId)
                    except PostUpdateFailedException:
                        Log.error("Failed to update Post after entry")
                        await botUtil.sendNotification(dlg.user, "Ein Fehler ist aufgetreten!", "Beim Bearbeiten des alten Posts ist es leider zu einem Fehler gekommen.")
                        return
                       
            elif type(dlg) is EditDialogue:
                if dlg.isDone():
                    guild = await self.getGuild(dlg.infoDict["guild"])
                    
                    channel = await self.getChannelForPost(dlg.infoDict, guild)

                    adminMsg = await self.getMessage(dlg.channel.id, dlg.adminPostId)
                    postMsg = await self.getMessage(*dlg.infoDict["post"])

                    if dlg.shouldNotify:
                        
                        try:
                            await self.createPost(dlg.infoDict, channel, dlg.user)
                        except PostCreationFailedException:
                            Log.error("Failed to create new Post after edit! Aborting.")
                            await botUtil.sendNotification(dlg.user, "Ein Fehler ist aufgetreten!", "Beim Übertragen der Änderung ist es leider zu einem Fehler gekommen.")
                            return

                        await botUtil.tryDelete(postMsg, adminMsg)

                        self.posts.pop(postMsg.id)
                        self.adminPosts.pop(adminMsg.id)

                    else:
                        self.posts[dlg.infoDict["post"][1]] = dlg.infoDict.copy()
                        
                        self.adminPosts[dlg.adminPostId] = dlg.infoDict.copy()

                        await self.updatePost(postMsg.id)
                        await self.updateAdminPost(adminMsg)
                    
            elif type(dlg) is AnnounceDialogue:
                if dlg.isDone():
                    
                    guild = await self.getGuild(dlg.infoDict["guild"])

                    if guild is None:
                        Log.warning("Could not get guild for post on announceDialogueClose!")
                        return
                    
                    # Posting the new Post

                    newDict = dlg.infoDict.copy()

                    newDict["type"] = dlg.type
                    
                    newDict["dates"] = dlg.chosenDates
                    newDict["entries"] = dlg.parts
                    
                    channel = await self.getChannelForPost(newDict, guild)

                    try:
                        await self.createPost(newDict.copy(), channel, dlg.user)
                    except PostCreationFailedException:
                        Log.error("Failed to announce a Post! Aborting.")
                        await botUtil.sendNotification(dlg.user, "Ein Fehler ist aufgetreten!", "Beim Erstellen des Kalendereintrages ist es leider zu einem Fehler gekommen.")
                        return

                    # Deleting the old Post if necessary

                    if dlg.infoDict["dates"] is None or len(dlg.infoDict["dates"]) == 0:
                        adminMsg = await self.getMessage(dlg.channel.id, dlg.adminPostId)
                        postMsg = await self.getMessage(*dlg.infoDict["post"])
                        await botUtil.tryDelete(postMsg, adminMsg)

                        self.posts.pop(postMsg.id)
                        self.adminPosts.pop(adminMsg.id)

                    # Updating the old Post otherwise

                    else:
                        self.posts[dlg.infoDict["post"][1]] = dlg.infoDict.copy()

                        self.adminPosts[dlg.adminPostId] = dlg.infoDict.copy()

                        try:
                            await self.updatePost(dlg.infoDict["post"][1])
                        except PostUpdateFailedException:
                            Log.error("Failed to update the old post after announcment!")
                            await botUtil.sendNotification(dlg.user, "Ein Fehler ist aufgetreten!", "Beim Bearbeiten des alten Posts ist es leider zu einem Fehler gekommen.")
                            return
                

            elif type(dlg) is MessageDialogue:
                if dlg.isDone():
                    tasks = []
                    for uId in dlg.recips:
                        user = await self.getUser(uId)
                        tasks.append(
                            botUtil.sendNotification(
                                recp=user,
                                title="Benachrichtigung: " + dlg.roundName,
                                message=dlg.message,
                                author=dlg.user
                            )
                        )
                    await asyncio.gather(
                        *tasks
                    )
            
            elif type(dlg) is KickDialogue:
                if dlg.isDone():
                    postId = dlg.infoDict["post"][1]

                    if dlg.infoDict["type"]:
                        self.posts[postId]["entries"] = dlg.infoDict["entries"]
                    else:
                        self.posts[postId]["dates"] = dlg.infoDict["dates"]
                    
                    users = await asyncio.gather(
                        *[
                            self.getUser(uid)
                            for uid in dlg.kickedUsers
                        ]
                    )
                    
                    await asyncio.gather(
                        *[
                            botUtil.sendNotification(
                                user,
                                "Du wurdest ausgetragen.",
                                "<@" + str(dlg.infoDict["user"]) + "> hat dich aus seiner Runde **" + dlg.infoDict["name"] + "** ausgetragen."
                            )
                            for user in users
                        ]
                    )

                    await self.updatePost(postId)

        except Exception:
            Log.exception("Exception occured in onDialogueClose")
        finally:
            self.dialogues.pop(channelId)

    async def createPost(self, infoDict, channel, author):
        try:
            mentions = ["<@" + str(infoDict["user"]) + ">"]

            if infoDict["type"]:
                if len(infoDict["entries"]) > 0:
                    mentions[0] += ' | '
                    mentions += ["<@" + str(userId) + ">" for userId in infoDict["entries"]]
            else:
                parts = []
                for dateParts in infoDict["dates"].values():
                    for part in dateParts:
                        if not part in parts:
                            parts.append(part)
                
                if len(parts) > 0:
                    mentions[0] += ' | '
                    mentions += ["<@" + str(userId) + ">" for userId in parts]


            mentions = ' '.join(mentions)
            
            msg = None

            embed = await self.toPublicPostEmbed(infoDict)

            if embed is None:
                Log.error("Aborting post creation, due to failed embed creation.")
                raise PostCreationFailedException()

            try:
                msg = await channel.send(content=mentions, embed=embed)
            except:
                try:
                    await asyncio.sleep(10)
                    msg = await author.send(None, embed=embed)
                except:
                    Log.exception("Failed to send post. Aborting.")
                    raise PostCreationFailedException()

            if msg is None:
                Log.error("Failed to create post for unknown reason! msg is None.")
                raise PostCreationFailedException()
            
            if infoDict["type"]:
                await msg.add_reaction("✅")
                await msg.add_reaction("❌")
            else:
                await msg.add_reaction("📝")
            
            infoDict["post"] = [msg.channel.id, msg.id]

            self.posts[msg.id] = infoDict
            fileSave.save(self.posts, fileName="data/Posts.json")
            
            try:
                await self.createAdminPosts(author, infoDict)
            except PostCreationFailedException:
                Log.error("Failed to create admin post during post creation!")
                raise PostCreationFailedException()


            Log.status("Created type " + str(infoDict["type"]) + " post '" + infoDict["name"] + "' with msgId " + str(msg.id))
        except Exception:
            Log.exception("Exception occured when creating Post")
            raise PostCreationFailedException()
    
    async def createAdminPosts(self, author, infoDict):
        try:
            
            msg = None

            embed = await self.toAdminPost(infoDict)

            if embed is None:
                Log.error("Aborting adminPost creation, due to failed embed creation!")
                raise PostCreationFailedException()

            try:
                msg = await author.send(None, embed=embed)
            except:
                try:
                    await asyncio.sleep(10)
                    msg = await author.send(None, embed=embed)
                except:
                    Log.exception("Failed to send admin Post. Aborting.")
                    raise PostCreationFailedException()

            if msg is None:
                Log.error("Failed to create admin post for unknown reason! msg is None.")
                raise PostCreationFailedException()
            
            await msg.add_reaction("✏️")
            await msg.add_reaction("👥")
            await msg.add_reaction("📻")
            await msg.add_reaction("🗑️")

            if infoDict["type"] < 2:
                await msg.add_reaction("📯")

            self.adminPosts[msg.id] = infoDict
            fileSave.save(self.adminPosts, fileName="data/AdminPosts.json")
        except Exception:
            Log.exception("Exception occured when creating Admin Post")
            raise PostCreationFailedException()


    async def updatePost(self, msgId):
        try:
            Log.status("Creating updated embed...")
            msg = await self.getMessage(*self.posts[msgId]["post"])

            if msg is None:
                return

            mentions = ["<@" + str(self.posts[msgId]["user"]) + ">"]

            if self.posts[msgId]["type"]:
                if len(self.posts[msgId]["entries"]) > 0:
                    mentions[0] += ' | '
                    mentions += ["<@" + str(userId) + ">" for userId in self.posts[msgId]["entries"]]
            else:
                parts = []
                for dateParts in self.posts[msgId]["dates"].values():
                    for part in dateParts:
                        if not part in parts:
                            parts.append(part)
                
                if len(parts) > 0:
                    mentions[0] += ' | '
                    mentions += ["<@" + str(userId) + ">" for userId in parts]
            
            embed = await self.toPublicPostEmbed(self.posts[msgId])

            if embed is None:
                Log.error("Aborting post update, due to failed embed creation!")
                raise PostUpdateFailedException()
            
            try:
                Log.status("Updating old post...")
                await msg.edit(content = ' '.join(mentions), embed = embed)
            except:
                try:
                    Log.warning("Failed to edit old message. Retrying.")
                    await asyncio.sleep(10)
                    await msg.edit(content = ' '.join(mentions), embed = embed)
                except:
                    Log.exception("Failed to edit old message. Aborting.")
                    raise PostUpdateFailedException()

            fileSave.save(self.posts, fileName="data/Posts.json")

            Log.status("Updated post '" + self.posts[msgId]["name"] + "' with msgId " + str(msg.id))
        except:
            Log.exception("Exception occured when updating Post")
            raise PostUpdateFailedException()

    async def updateAdminPost(self, msg):
        try:
            if msg is None or not msg.id in self.adminPosts:
                return

            infoDict = self.adminPosts[msg.id]
            
            embed = await self.toAdminPost(infoDict)

            if embed is None:
                Log.error("Aborting adminPost update, due to failed embed creation!")
                raise PostUpdateFailedException()
            
            try:
                await msg.edit(embed=embed)
            except:
                try:
                    await asyncio.sleep(10)
                    await msg.edit(embed=embed)
                except:
                    Log.exception("Failed to edit old admin message. Aborting.")
                    raise PostUpdateFailedException()

            fileSave.save(self.adminPosts, fileName="data/AdminPosts.json")
        except:
            Log.exception("Exception occured when updating Admin Post")
            raise PostUpdateFailedException()


    async def getChannelForPost(self, infoDict, guild):
        
        channel = None

        if infoDict["type"] == 0:
            channel = await self.getDateChannel(guild.id)
        elif infoDict["type"] == 1:
            channel = await self.getPlayerChannel(guild.id)
        elif infoDict["type"] == 2:
            channel = await self.getCalendarChannel(guild.id)
        else:
            raise ValueError("Invalid type: " + str(infoDict["type"]))
        
        if channel is None and "post" in infoDict:
                channel = await self.getChannel(infoDict["post"][0])
        
        return channel
                    

    async def toPublicPostEmbed(self, infoDict):
        try:
            user = await self.getUser(infoDict["user"])

            col = discord.Color(0xf0f0f0)

            notTask = None

            if len(infoDict["dates"]) > 0 and (float(infoDict["dates"][-1]) < time() if infoDict["type"] > 0 else float(sorted(infoDict["dates"].keys())[-1]) < time()):
                col = discord.Color(0x000000)
                if not "autodel" in infoDict:
                    infoDict["autodel"] = time() + 259200.0
                    msg = None
                    try:
                        msg = await self.getMessage(*infoDict["post"])
                    except:
                        Log.warning("Failed to find message for auto delete.")

                    notTask = asyncio.create_task(
                        botUtil.sendNotification(
                            user,
                            "Auto-Delete: **" + infoDict["name"] + "**",

                            "Der Post für **" +
                            infoDict["name"] +
                            "** wird in 72h automatisch gelöscht, weil alle Termine in der Vergangenheit liegen. Wenn du z.B. die Beschreibung aufheben willst kannst du sie dir noch kopieren. " +
                            "Wenn du nicht willst, dass die nachricht gelöscht wird, kannst du neue Termine hinzufügen oder alle Termine entfernen." +
                            ( "\n\nDen Post findest du hier: " + msg.jump_url if msg is not None else "" )
                        )
                    )
            else:
                if "autodel" in infoDict:
                    infoDict.pop("autodel")

                    msg = await self.getMessage(*infoDict["post"])

                    notTask = asyncio.create_task(
                        botUtil.sendNotification(
                            user,
                            "Auto-Delete Abgebrochen: **" + infoDict["name"] + "**",

                            "Der Post für **" +
                            infoDict["name"] +
                            "** wird nicht mehr automatisch gelöscht." +
                            "\n\nDen Post findest du hier: " +
                            msg.jump_url
                        )
                    )
                
                if infoDict["type"] == 1:
                    if infoDict["limit"] is None or len(infoDict["entries"]) < infoDict["limit"]:
                        col = discord.Color(0x55ff00)
                    else:
                        col = discord.Color(0xff0033)

#discord.Color(0xfd0061)

            embd = discord.Embed(title = "*" + infoDict["system"] + "*\n" + infoDict["name"], color=col, description=infoDict["desc"]) \
                .set_thumbnail(url = self.user.avatar.url if self.user.avatar is not None else None) \
                .set_author(name = user.name, icon_url = user.avatar.url if user.avatar is not None else None)
            
            if infoDict["img"] is not None:
                try:
                    embd.set_image(url=infoDict["img"])
                except:
                    Log.warning("Failed to load image for embed.")
                    await botUtil.sendNotification(user, "Ein Fehler ist aufgetreten!", "Ich konnte das Bild nicht laden, es scheint etwas mit der Bild URL nicht zu stimmen. Wenn du es nochmal versuchen willst oder eine anderes Bild hast, kannst du das über 'Bearbeiten' nochmal ein Bild angeben.")
            
            if not infoDict["info"] is None and infoDict["info"] != "":
                embd.add_field(name = "Anmerkungen", value = infoDict["info"], inline = False)
            
            if infoDict["type"]:
            
                embd.set_footer(text = "Drück auf ✅ und ❌, um dich ein- und auszutragen.")

                if len(infoDict["dates"]) == 0:
                    embd.add_field(name = "Termin", value="???", inline = False)

                elif len(infoDict["dates"]) == 1:
                    dt = datetime.datetime.fromtimestamp(float(infoDict["dates"][0]))
                    
                    showTime = dt.time() != datetime.time(0, 0, 0, 0)
                    
                    embd.add_field(name = "Termin", value=dateTimeConv.dateToStr(dt.date()), inline = showTime)

                    if showTime:
                        embd.add_field(name = "Uhrzeit", value=dateTimeConv.timeToStr(dt.time()), inline = True) \
                            .add_field(name="\u200b", value="\u200b", inline = True)
                else:
                    dates = []
                    for date in infoDict["dates"]:
                        dt = datetime.datetime.fromtimestamp(float(date))
                        dates.append(dateTimeConv.dateTimeToStr(dt))
                    dates = '\n'.join(dates)
                    
                    embd.add_field(name="Termine", value=dates, inline=False)

                
                part = []
                queue = []

                iter = 0
                limit = infoDict["limit"]
                for user in infoDict["entries"]:
                    iter += 1
                    if limit is None or iter <= limit:
                        part.append(' ' + str(iter) + '. ' + "<@" + str(user) + ">")
                    else:
                        queue.append(' ' + str(iter - limit) + '. ' + "<@" + str(user) + ">")
                
                partCount = len(part)
                queueCount = len(queue)

                if partCount > 0:
                    part = '\n'.join(part)
                else:
                    part = '-'
                
                if queueCount > 0:
                    queue = '\n'.join(queue)
                else:
                    queue = '-'

                partLimit = " (" + str(partCount) + "/" + str(limit) + ")" if not limit is None else ""

                embd.add_field(name = "Teilnehmer" + partLimit, value = part, inline = True)

                if not limit is None:
                    embd.add_field(name = "Warteliste (" + str(queueCount) + ")", value = queue, inline = True)
            else:
                embd.add_field(name = "Max. Teilnehmer:innen", value = (str(infoDict["limit"]) if not infoDict["limit"] is None else "♾️"), inline = False) \
                    .set_footer(text = "Drück auf den 📝, um dich einzutragen.")
            
                for date, users in infoDict["dates"].items():
                    v = None
                    if len(users) == 0:
                        v = "-"
                    else:
                        v = []
                        iter = 0
                        for user in users:
                            iter += 1
                            v.append(' ' + str(iter) + '. ' + "<@" + str(user) + ">")
                        if not infoDict["limit"] is None:
                            limit = int(infoDict["limit"])
                            if len(v) > limit:
                                v.insert(limit, '**------------**')
                        v = '\n'.join(v)

                    dt = datetime.datetime.fromtimestamp(float(date))

                    embd.add_field(name = dateTimeConv.dateTimeToStr(dt), value = v, inline = True)
                
                if len(infoDict["dates"]) > 3 and len(infoDict["dates"]) % 3 != 0:
                    for i in range(3 - (len(infoDict["dates"]) % 3)):
                        embd.add_field(name="\u200b", value="\u200b", inline = True)
            
            if notTask is not None:
                await notTask

            return embd
        except Exception as ex:
            Log.exception(ex)
            return None

            
        #return "**" + ("<@" + str(infoDict["user"]) + ">" + " bietet eine Runde *" + infoDict["system"] + "* an!**\n\n" + botUtil.makeQuoted("**" + infoDict["name"] + ":**\n\n**Beschreibung:**\n" + infoDict["desc"] + "\n\n**Termine:**\n" + '\n'.join(infoDict["dates"]))

    async def toAdminPost(self, infoDict):

        res = "Du hast " + ("**den Kalendereintrag**" if infoDict["type"] == 2 else ("die **Spielendensuche**" if infoDict["type"] == 1 else "die **Terminfindung**"))
        res += " *" + infoDict["name"] + "* (*" + infoDict["system"] + "*) ausgehangen!\n**Drück auf:**\n  ✏️ um deine Angaben zu verändern\n  👥 um die eingetragenen Mitspieler:innen zu verwalten\n  📻 um den eingetragenen Teilnehmer:innen eine Nachricht zu schicken\n  🗑️ um den Post zu löschen\n"
        if infoDict["type"] == 0:
            res += "📯 um den/die *Termine festzulegen*\n"
        elif infoDict["type"] == 1:
            res += "📯 um den/die Termine in den Kalender zu verschieben\n"
        
        res += '\n'

        res += (await self.getMessage(*infoDict["post"])).jump_url

        return discord.Embed(
            title="Kalendereintrag" if infoDict["type"] == 2 else ("Spielendensuche" if infoDict["type"] == 1 else "Terminfindung"),
            description=res,
            color=discord.Color(0xfd0061)
        )

with Log() as l:
    bot = WieselBot(intents=discord.Intents.all())
    bot.run(discordToken.getToken())

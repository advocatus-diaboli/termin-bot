from datetime import date, time, datetime

from log import Log

def dateToStr(dt):
    weekdays = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
    weekday = weekdays[dt.weekday()] + '. '

    return weekday + dt.strftime("%d.%m.%y") + '\t\u200b'

def timeToStr(tm):
    return tm.strftime("%H:%M")

def dateTimeToStr(dt):
    weekdays = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
    weekday = weekdays[dt.weekday()] + '. '

    if dt.time() == time(0, 0, 0, 0):
        return weekday + dt.strftime("%d.%m.%y") + '\t\u200b'
    return weekday + dt.strftime("%d.%m.%y / %H:%M") + '\t\u200b'

def strToDateTime(text):
    dt = None
    tm = None

    if len(text) == 0:
        return None

    try:
        splitted = text.split(' ')
        splitted = list(filter(None, splitted))
        
        months = {
            'januar'    : "1",
            'jan'       : "1",
            'februar'   : "2",
            'feb'       : "2",
            'märz'      : "3",
            'april'     : "4",
            'apr'       : "4",
            'mai'       : "5",
            'juni'      : "6",
            'juli'      : "7",
            'august'    : "8",
            'aug'       : "8",
            'september' : "9",
            'sep'       : "9",
            'oktober'   : "10",
            'okt'       : "10",
            'november'  : "11",
            'nov'       : "11",
            'dezember'  : "12",
            'dez'       : "12"
        }

        if len(splitted) > 2:
            if splitted[-1] == "Uhr":
                splitted = splitted[:-1]
        
        if len(splitted) > 1:
            if splitted[1].lower() in months:
                splitted[0] += '.' + str(months[splitted[1].lower()])
                splitted.pop(1)
            elif splitted[1][:-1].lower() in months:
                splitted[0] += '.' + str(months[splitted[1][:-1].lower()])
                splitted.pop(1)


        if len(splitted) <= 2:
            dateElems = splitted[0].split('.')
            dateElems = list(filter(None, dateElems))
            
            if len(dateElems) <= 1 or len(dateElems) > 3:
                return None

            y = None
            if len(dateElems) == 3:
                y = int(dateElems[2])
                if y < 0:
                    return None
                elif y < 100:
                    y += 2000
            else:
                y = date.today().year
            
            dt = date(year=y, month=int(dateElems[1]), day=int(dateElems[0]))

            if dt < date.today() and len(dateElems) == 2:
                dt = dt.replace(year=y+1)

            if len(splitted) == 2:
                timeElems = splitted[1].split(':')
                if len(timeElems) == 1:
                    tm = time(hour=int(timeElems[0]), minute=0)
                elif len(timeElems) == 2:
                    tm = time(hour=int(timeElems[0]), minute=int(timeElems[1]))
                else:
                    return None
        else:
            return None
        
        if tm is None:
            tm = time()
        
        return datetime.combine(dt, tm)
    except Exception as ex:
        Log.warning("Str to datetime Conversion failed: " + str(ex))
        return None
import inspect

from log import Log

from time import time

class EventError(Exception):
    pass

class EventLink:
    events = {}

    def registerEvent(self, id, function):
        if id in self.events:
            raise EventError("This event is already registered")
        self.events[id] = (function, time())
        Log.status("Registered event with id: " + str(id));
    
    def unregisterEvent(self, id):
        if not id in self.events:
            Log.warning("Cannot unregister event that doesn't exist: " + str(id));
            return;
        self.events.pop(id)
        Log.status("Unregistered event with id: " + str(id));

    def __contains__(self, id):
        return id in self.events
    
    async def raiseEvent(self, id, *args):
        if not id in self.events:
            raise EventError("This event is not registered")
        Log.status("Raising event with id: " + str(id));
        await self.events[id][0](*args)

    async def tryRaise(self, id, *args):
        try:
            await self.raiseEvent(id, *args)
        except EventError as ex:
            Log.warning("Failed to raise Event with id " + str(id) + "\n\tMessage:\t" + str(ex))
            return False
        return True
    
    def filterEvents(self, olderThanMinutes = 1440):
        remove = []
        now = time()
        for id, tup in self.events.items():
            if now - tup[1] > olderThanMinutes * 60:
                remove.append(id)
        
        for id in remove:
            self.events.pop(id)
        
        return len(remove)

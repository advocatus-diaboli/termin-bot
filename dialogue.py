from abc import abstractmethod

import botUtil

from log import Log

class Dialogue:
    user = None
    channel = None
    eventLink = None

    eventBuffer = []
    
    done = False

    valid = False

    pos = 0

    headerMsg = None
    lastMsg = None

    instanceId = None
    lastId = -1

    def __init__(self, eventLink, user, channel):
        self.eventLink = eventLink
        self.user = user
        self.instanceId = Dialogue.lastId
        self.channel = channel

        self.done = False
        
        Dialogue.lastId = Dialogue.lastId - 1

        Log.status("Constructed " + self.getDialogueType() + " with User " + user.name  + '#' + user.discriminator + " (Dialogue ID " + str(self.instanceId) + ")")

    def getId(self):
        return self.instanceId

    def isValid(self):
        return not self.user is None and not self.eventLink is None and not self.channel is None

    def isDone(self):
        return self.done

    def isClosed(self):
        return self.pos == -1
    
    async def close(self):
        if self.isClosed():
            return
        self.pos = -1
        self.eventLink.unregisterEvent(self.headerMsg.id)

        await botUtil.tryDelete(self.lastMsg, self.headerMsg)

        Log.status("Raising dialogueCloseEvent for " + self.getDialogueType() + " with User " + self.user.name  + '#' + self.user.discriminator + " (Dialogue ID " + str(self.instanceId) + ")")

        if self.instanceId in self.eventLink:
            await self.eventLink.raiseEvent(self.instanceId, self.channel.id)
        
        Log.status("Closed " + self.getDialogueType() + " with User " + self.user.name + '#' + self.user.discriminator + " (Dialogue ID " + str(self.instanceId) + ")")
        Log.emptyLine()

        
    
    async def onHeaderReaction(self, wasSet, emoji, msg, user):
        if self.isClosed() or user != self.user or not wasSet:
            return True
        if emoji == "❌":
            await self.close()
            return True
        return False

    async def start(self, primaryText, subText = "", reactions = None):
        if not self.isValid():
            Log.error("Tried to start invalid " + self.getDialogueType())
            raise ValueError("The dialogue is invalid.")
        if primaryText is None:
            primaryText = ""
        if subText is None:
            subText = ""
        msg = await self.channel.send(None, embed = botUtil.createBasicEmbed(title = "Puudel", author = None, fieldTitle = primaryText, fieldText = subText + "\nDrück auf das ❌ um den Dialog abzubrechen."))

        if not reactions is None:
            if type(reactions) is list:
                for r in reactions:
                    await msg.add_reaction(r)
            else:
                await msg.add_reaction(reactions)
        await msg.add_reaction("❌")
        self.headerMsg = msg

        self.eventLink.registerEvent(self.headerMsg.id, self.onHeaderReaction)
        
        await self.sendNext()
        
        return

    def clearEventBuffer(self):
        for evId in self.eventBuffer:
            if evId in self.eventLink:
                self.eventLink.unregisterEvent(evId)

        self.eventBuffer = []

    @abstractmethod
    async def sendNext(self):
        pass

    def nextPos(self):
        self.pos = self.pos + 1

    def getChannel(self):
        if not self.isValid():
            Log.error("Tried to get channel of invalid " + self.getDialogueType())
            raise ValueError("The dialogue is invalid.")
        return self.channel
    
    def getDialogueType(self):
        return "BaseDialogue"

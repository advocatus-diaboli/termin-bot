import dialogue

import botUtil
import datetime
import dateTimeConv

import discord

class AnnounceDialogue(dialogue.Dialogue):
    
    infoDict = {}
    adminPostId = None

    chosenDate = None
    parts = None
    partials = None
    type = 1

    dateMsgs = {}

    def __init__(self, eventLink, user, channel, infoDict, adminPostId):
        super().__init__(eventLink, user, channel)

        self.infoDict = infoDict.copy()
        self.adminPostId = adminPostId
        self.dateMsgs = {}

        self.chosenDates = []
        self.parts = None
        self.partials = []
        self.type = 1
    
    async def start(self):
        await super().start(
            primaryText="Sag den oder die Termine an!\nAlle angesagten Termine werden dann aus dem ursprünglichen Aushang entfernt.",
            subText="Drück auf 👍, sobald du alle Termine ausgewählt hast.",
            reactions='👍'
        )

    async def sendNext(self):
        if not self.isValid():
            return
        
        msg = []

        if self.pos == 0:
            for date in self.infoDict["dates"]:
                
                dt = datetime.datetime.fromtimestamp(float(date))

                tmp = await self.channel.send(dateTimeConv.dateTimeToStr(dt))
                await tmp.add_reaction('📯')

                self.eventLink.registerEvent(tmp.id, self.onChoice)

                msg.append(tmp)

                self.dateMsgs[tmp.id] = date
        elif self.pos == 1:
            msg = await self.channel.send(None,
                embed=discord.Embed(
                    title="Es können einige Spielende nicht an allen ausgewählten Terminen.",
                    description="Sollen sie trotzdem in den neuen Post übernommen werden?\nWähle nur ✅, wenn du dir sicher bist, dass die betroffenen Personen damit einverstanden sind.",
                    color=discord.Color(0xfd0061)
                )
            )
            await msg.add_reaction("✅")
            await msg.add_reaction("❌")

            self.eventLink.registerEvent(msg.id, self.onChoice)
        elif self.pos == 2:
            msg = await self.channel.send(None,
                embed=botUtil.createBasicEmbed(
                    title="Sollen die Mitspielenden aus den anderen Terminen ausgetragen werden? 🗑️ um den alten Post ganz zu löschen."
                )
            )
            await msg.add_reaction("✅")
            await msg.add_reaction("❌")
            await msg.add_reaction("🗑️")
            
            self.eventLink.registerEvent(msg.id, self.onChoice)
        elif self.pos == 3:
            msg = await self.channel.send(None,
                embed=botUtil.createBasicEmbed(
                    title="Suchst du noch nach Spielenden für den Termin (👥) oder soll er in den Kalender (📯)?."
                )
            )
            await msg.add_reaction("👥")
            await msg.add_reaction("📯")

            self.eventLink.registerEvent(msg.id, self.onChoice)
        else:
            return
        
        self.lastMsg = msg
    
    async def onChoice(self, wasSet, emoji, msg, user):
        if self.pos == 0 and emoji == '📯':
            
            if wasSet:
                if msg.id not in self.chosenDates:
                    self.chosenDates.append(msg.id)
            else:
                if msg.id in self.chosenDates:
                    self.chosenDates.remove(msg.id)
            
            return
            
            
        if not wasSet:
            return
        
        if self.pos == 1:
            if emoji == "✅":
                self.parts += self.partials
            elif emoji != "❌":
                return
            
            await botUtil.tryDelete(self.lastMsg)

            self.eventLink.unregisterEvent(msg.id)
            
            self.nextPos()
            await self.sendNext()

        elif self.pos == 2:
            if emoji == "✅":
                for part in self.parts:
                    for partList in self.infoDict["dates"].values():
                        if part in partList:
                            partList.remove(part)
            elif emoji == "🗑️":
                self.infoDict["dates"] = None
            elif emoji != "❌":
                return
            
            await botUtil.tryDelete(self.lastMsg)
        
            self.eventLink.unregisterEvent(msg.id)
            
            self.nextPos()
            await self.sendNext()
        
        elif self.pos == 3:
            if emoji == "📯":
                self.type = 2
            elif emoji != "👥":
                return
                
            await botUtil.tryDelete(self.lastMsg)
        
            self.eventLink.unregisterEvent(msg.id)
            
            self.done = True
            await self.close()
    
    async def onHeaderReaction(self, wasSet, emoji, msg, user):
        if not await super().onHeaderReaction(wasSet, emoji, msg, user):
            if wasSet and emoji == '👍' and self.pos == 0:
                for msg in self.dateMsgs:
                    self.eventLink.unregisterEvent(msg)

                for i in range(len(self.chosenDates)):
                    date = self.dateMsgs[self.chosenDates[i]]
                    self.chosenDates[i] = date

                    if self.parts is None:
                        self.parts = self.infoDict["dates"][date]
                    else:
                        dateParts = self.infoDict["dates"][date]

                        for p in self.parts:
                            if p in dateParts:
                                continue
                            if not p in self.partials:
                                self.partials.append(p)
                            self.parts.remove(p)
                    
                    self.infoDict["dates"].pop(date)

                await botUtil.tryDelete(self.lastMsg)

                if len(self.partials) > 0:
                    self.pos = 1
                else:
                    self.pos = 2

                await self.sendNext()
                
    def getDialogueType(self):
        return "AnnounceDialogue"
    
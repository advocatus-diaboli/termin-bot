import json
import os

from log import Log

def save(dictionary, fileName):
    Log.status("Saving file at '", fileName, "'")
    os.makedirs(os.path.dirname(fileName), exist_ok=True)
    with open(fileName, "w") as outFile:
        json.dump(dictionary, outFile, indent=4)
    Log.status("Saved file at '", fileName, "'")

def load(fileName):
    tmp = None
    try:
        with open(fileName, "r") as inFile:
            tmp = json.load(inFile)
    except FileNotFoundError:
        Log.warning("Could not find file at: " + str(fileName))
        return {}
    except Exception as ex:
        Log.exception(ex)
        Log.status("Returning file content simply as '{}'")
        return {}
    
    res = {}

    for msgId, data in tmp.items():
        res[int(msgId)] = data

    Log.status("Loaded file at '", fileName, "'")

    return res

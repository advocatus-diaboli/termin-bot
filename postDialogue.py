from discord import message
from dialogue import Dialogue

import botUtil
import dateTimeConv
import io

from datetime import date, time, datetime

import discord


class PostDialogue(Dialogue):
    tmpMsg = None

    postName = None
    postSys = None
    postDesc = None
    postInfo = None
    postLimit = None
    postDates = []
    postType = 0
    postReservations = []
    postImage = None

    imgCache = None

    def __init__(self, eventLink, user, channel, tmpMsg, name, imgCache):
        super().__init__(eventLink, user, channel)
        self.tmpMsg = tmpMsg
        self.postName = name
        self.postSys = None
        self.postLimit = None
        self.postDesc = None
        self.postInfo = None
        self.postDates = []
        self.postType = 0
        self.postReservations = []
        self.postImage = None

        self.imgCache = imgCache
    
    async def start(self):
        if not self.postName is None:
            self.nextPos()
        
        await super().start(primaryText="Bitte gib die Daten für deine Runde an.")
        self.eventLink.registerEvent(self.channel.id, self.onMessage)

    async def sendNext(self):
        if not self.isValid():
            raise ValueError("The dialogue is invalid.")
        msg = None
        if self.pos == 0:
            msg = await self.channel.send(None, embed=discord.Embed(title="Titel / Szenario:", color=discord.Color(0xfd0061)))
        elif self.pos == 1:
            msg = await self.channel.send(None, embed=discord.Embed(title="System:", color=discord.Color(0xfd0061)))
        elif self.pos == 2:
            msg = await self.channel.send(None, embed=discord.Embed(title="Beschreibung: (Max. 2000 Zeichen)", color=discord.Color(0xfd0061)))
        elif self.pos == 3:
            msg = await self.channel.send(None, embed=discord.Embed(title="Anmerkungen: (Max. 1000 Zeichen)\n(Drück auf '⏭', zum überspringen.)", color=discord.Color(0xfd0061)))
            await msg.add_reaction('⏭')
            self.eventLink.registerEvent(msg.id, self.onOtherReaction)
        elif self.pos == 4:
            msg = await self.channel.send(None, embed=discord.Embed(title="Bild:  (Bevorzugt als Link, sonst als Anhang) (Max. 1 MB)\n(Drück auf '⏭', zum überspringen.)", color=discord.Color(0xfd0061)))
            await msg.add_reaction('⏭')
            self.eventLink.registerEvent(msg.id, self.onOtherReaction)

            self.lastMsg = [msg]
            return
        elif self.pos == 5:
            msg = await self.channel.send(None, embed=discord.Embed(title="Max. Teilnehmer:innen: (Drück auf '⏭', zum überspringen.)", color=discord.Color(0xfd0061)))
            await msg.add_reaction('⏭')
            self.eventLink.registerEvent(msg.id, self.onOtherReaction)
        elif self.pos == 6:
            msg = await self.channel.send(None, embed=discord.Embed(title="Termin(e): (Max. 21)", description="Format: *'Datum Uhrzeit'* oder nur *'Datum'*.\nBeispiele: '1.1 11:11', '22.2.22 11 Uhr', '1. Januar, 14:00'\nDrück auf '👍', wenn du fertig bist.", color=discord.Color(0xfd0061)))
            await msg.add_reaction('👍')

            self.eventLink.registerEvent(msg.id, self.onOtherReaction)
            
            self.lastMsg = [msg]
            return
        elif self.pos == 7:
            msg = await self.channel.send(None, embed=discord.Embed(title="Suchst du Termine (und Spielende) (*Terminfindung:* 🗓️), nur Spielende (*Spielendensuche:* 👥) oder soll der Post in den Kalender (*Kalendereintrag:* 📯)?", color=discord.Color(0xfd0061)))
            await msg.add_reaction('🗓️')
            await msg.add_reaction('👥')
            await msg.add_reaction('📯')

            self.eventLink.registerEvent(msg.id, self.onOtherReaction)
        elif self.pos == 8:
            msg = await self.channel.send(None, embed=discord.Embed(title="Reservierte Plätze:", description="Falls einige Teilnehmer:innen schon feststehen und du sicher gehen willst, dass sie auch ganz sicher einen Platz kriegen, kannst du sie hier eintragen. Sie werden dann für ALLE Termine eingetragen und müssen sich ggf. dann noch 'umtragen'. Bitte gib die Teilnehmer:innen als Mention (also mit @) an.\n\nDrück auf '👍', wenn du fertig bist.\n\n**Achtung:** *Es ist* **NICHT** *möglich nach der Erstellung noch Plätze zu reservieren!*", color=discord.Color(0xfd0061)))
            await msg.add_reaction('👍')

            self.eventLink.registerEvent(msg.id, self.onOtherReaction)
            
            self.lastMsg = [msg]
            return
        else:
            self.done = True
            await self.close()
        self.lastMsg = msg

    async def onMessage(self, msg):
        if not self.isValid():
            raise ValueError("The dialogue is invalid.")
        if msg.author != self.user:
            await botUtil.tryDelete(msg)
            return
        
        if self.pos == 0:
            self.postName = msg.content
        elif self.pos == 1:
            self.postSys = msg.content
        elif self.pos == 2:
            if len(msg.content) > 2000:
                if not type(self.lastMsg) is list:
                    self.lastMsg = [self.lastMsg]
                tmp = await self.channel.send("*Die Beschreibung ist mit " + str(len(msg.content)) + " Zeichen leider zu lang*")
                self.lastMsg.append(tmp)
                self.lastMsg.append(msg)
                return
            self.postDesc = msg.content
        elif self.pos == 3:
            if len(msg.content) > 1000:
                if not type(self.lastMsg) is list:
                    self.lastMsg = [self.lastMsg]
                tmp = await self.channel.send("*Die Anmerkung ist mit " + str(len(msg.content)) + " Zeichen leider zu lang*")
                self.lastMsg.append(tmp)
                self.lastMsg.append(msg)
                return
            self.postInfo = msg.content
        elif self.pos == 4:
            if msg.content == "":
                if len(msg.attachments) == 0:
                    tmp = await self.channel.send("*Ich habe leider kein Bild in deiner Nachricht gefunden*")
                    self.lastMsg.append(tmp)
                    self.lastMsg.append(msg)
                    return
                else:
                    if msg.attachments[0].size > 0xfffff:
                        tmp = await self.channel.send("*Der Anhang ist zu groß, das Bild darf maximal **1 MB** groß sein*")
                        self.lastMsg.append(tmp)
                        self.lastMsg.append(msg)
                        return
                    else:
                        #path = imgPath.getPath()
                        #await msg.attachments[0].save(path)
                        #self.postImage = '@' + str(path)

                        if self.imgCache is None:
                            tmp = await self.channel.send("*Ich kann auf diesem Server im Moment keine Bilder zwischenspeichern, bitte schick mir ein Bild als Link*")
                            self.lastMsg.append(tmp)
                            self.lastMsg.append(msg)
                            return

                        imgData = await msg.attachments[0].read()
                        bs = io.BytesIO(imgData)
                        imgFile = discord.File(bs, filename=msg.attachments[0].filename)

                        tmp = await self.imgCache.send("Bild für: **" + self.postName + "**", file=imgFile)
                        self.postImage = tmp.attachments[0].url
                        
            else:
                cont = msg.content.strip()
                if not (cont.startswith("http://") or cont.startswith("https://")):
                    tmp = await self.channel.send("*Bitte stell sicher, dass die Bild URL ordnungsgemäß ist und mit 'http://' oder 'https://' anfängt.*")
                    self.lastMsg.append(tmp)
                    self.lastMsg.append(msg)
                    return
                self.postImage = cont
        elif self.pos == 5:
            try:
                self.postLimit = int(msg.content)
                if self.postLimit <= 0:
                    self.postLimit = None
                    raise ValueError()
            except ValueError:
                if not type(self.lastMsg) is list:
                    self.lastMsg = [self.lastMsg]
                self.lastMsg.append(msg)
                msg = await self.channel.send("*Bitte gib eine Zahl (größer als 0) an*")
                self.lastMsg.append(msg)
                return
        elif self.pos == 6:
            dateEntries = msg.content.split('\n')
            errors = []
            invalids = []

            tday = datetime.now()

            if len(dateEntries) + len(self.postDates) > 21:
                tmp = await self.channel.send(
                    "*Das sind zu viele Termine*" + 
                    ("\nDu kannst noch maximal " + str(21 - len(self.postDates)) + "angeben." if len(self.postDates) < 21 else "")
                )
                return

            for entry in dateEntries:
                entry = entry.strip()

                if len(entry) == 0:
                    continue

                dt = dateTimeConv.strToDateTime(entry)

                if dt is None:
                    errors.append(entry)
                    continue

                if dt < tday and dt.time() != time(0, 0, 0, 0):
                    invalids.append(entry)
                    continue

                self.postDates.append(dt.timestamp())
            
            if len(errors) > 0:
                tmp = None
                if len(errors) == 1:
                    tmp = await self.channel.send("*Den Termin '" + errors[0] + "' hab ich leider nicht verstanden*")
                else:
                    tmp = await self.channel.send("*Die folgenden Termine habe ich nicht verstanden:*\n" + '\n'.join(errors))
                self.lastMsg.append(tmp)

            if len(invalids) > 0:
                tmp = None
                if len(invalids) == 1:
                    tmp = await self.channel.send("*Der Termin '" + invalids[0] + "' liegt in der Vergangenheit*")
                else:
                    tmp = await self.channel.send("*Die folgenden Termine liegen in der Vergangenheit:*\n" + '\n'.join(invalids))
                self.lastMsg.append(tmp)
            
            self.lastMsg.append(msg)
            return
        elif self.pos == 8:
            text = ''.join(msg.content.split())
            parsingErrors = []

            while len(text) > 0:
                if not text.startswith('<@'):
                    pos = text.find('<@')
                    if pos == -1:
                        parsingErrors.append(text)
                        break
                    
                    parsingErrors.append(text[:pos])
                    text = text[pos:]
                
                start = 2

                if text.startswith('<@!'):
                    start = 3
                
                pos = text.find('>')

                if pos == -1:
                    parsingErrors.append(text)
                    break
                
                try:
                    self.postReservations.append(int(text[start:pos]))
                except:
                    parsingErrors.append(text[:pos+1])

                text = text[pos+1:]

            self.lastMsg.append(msg)

            if len(parsingErrors) > 0:
                tmp = None
                if len(parsingErrors) == 1:
                    tmp = await self.channel.send("*Bitte gib den User '" + parsingErrors[0] + "' nochmal als Mention an*")
                else:
                    tmp = await self.channel.send("*Die folgenden Mentions haben scheinbar nicht funktioniert:*\n" + '\n'.join(parsingErrors))
                self.lastMsg.append(tmp)
            
            return

        else:
            return
        
        await botUtil.tryDelete(msg, self.lastMsg)

        self.nextPos()
        await self.sendNext()
    
    async def onOtherReaction(self, wasSet, emoji, msg, user):
        if not wasSet:
            return
        if emoji == '👍' or emoji == '⏭':
            await botUtil.tryDelete(self.lastMsg)
            self.nextPos()
            await self.sendNext()
        elif emoji == '🗓️' or emoji == '👥' or emoji == '📯':
            if emoji == '📯':
                self.postType = 2
            elif emoji == '👥':
                self.postType = 1
            await botUtil.tryDelete(self.lastMsg)
            self.nextPos()
            await self.sendNext()


    async def close(self):
        self.eventLink.unregisterEvent(self.channel.id)
        
        await super().close()
    
    def getDict(self):
        tmp = {
                "user" : self.user.id,
                "name" : self.postName,
                "system" : self.postSys,
                "desc" : self.postDesc,
                "info" : self.postInfo,
                "img" : self.postImage,
                "limit" : self.postLimit,
                "guild": self.channel.guild.id,
                "type": self.postType
            }
        
        if self.postType:
            tmp["dates"] = self.postDates
            tmp["entries"] = self.postReservations.copy()
        else:
            tmp["dates"] = {date:self.postReservations.copy() for date in sorted(self.postDates)}
        
        return tmp
    
    def getDialogueType(self):
        return "PostDialogue"
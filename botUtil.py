import discord
from discord import client
from discord.ext import commands

import asyncio

from log import Log

async def tryDelete(*args):
    def toBulk(args):
        res = {}
        for arg in args:
            if type(arg) is list:
                tmp = toBulk(arg)
                for key, elem in tmp.items():
                    if not key in res:
                        res[key] = []
                    res[key] += elem
            elif type(arg) is discord.Message:
                if not arg.channel in res:
                    res[arg.channel] = []
                res[arg.channel].append(arg)
        return res
    
    bulks = toBulk(args)

    for channel, messages in bulks.items():
        if type(channel) is discord.TextChannel:
            try:
                await channel.delete_messages(set(messages))
            except discord.NotFound:
                Log.warning("Message for deletion not found")
                continue
        else:
            await asyncio.gather(
                *[msg.delete() for msg in messages],
                return_exceptions=True
            )

def makeQuoted(text):
    return '> ' + '\n> '.join(text.split('\n'))

def createBasicEmbed(title=None, author=None, fieldTitle=None, fieldText=None, color=discord.Color(0xfd0061)):
    embd = discord.Embed(title = title, color = color)
    if not author is None:
        embd.set_author(name = author.name, icon_url = author.avatar.url if author.avatar is not None else None)
    if not (fieldTitle is None and fieldText is None):
        if fieldTitle is None:
            fieldTitle = "\u200b"
        if fieldText is None:
            fieldText = "\u200b"
        embd.add_field(name = fieldTitle, value = fieldText, inline = False)

    return embd

async def sendNotification(recp, title, message, author=None):
    try:
        Log.status("Sending notification '", title, "' to '", fullName(recp), "\tMessage:\n", message)
        Log.emptyLine()
        embd = discord.Embed(
            title=title,
            description=message
        ).set_footer(
            text="Drück auf 🔥 um diese Benachrichtigung zu löschen."
        )
        
        if author is not None:
            embd.set_author(name = author.name, icon_url = author.avatar.url if author.avatar is not None else None)

        tmp = await recp.send(
            None,
            embed=embd
        )
        await tmp.add_reaction("🔥")
    except:
        Log.exception("Failed to send Notification!")

def fullName(user):
    return user.name + '#' + user.discriminator
from dialogue import Dialogue

import asyncio

import botUtil

class KickDialogue(Dialogue):
    
    infoDict = {}
    msgs = {}
    kickedUsers = []
    willKick = []

    def __init__(self, eventLink, user, channel, infoDict):
        super().__init__(eventLink, user, channel)

        self.infoDict = infoDict.copy()
        self.userMsgs = {}
        self.lastMsg = []
        self.kickedUsers = []
        self.willKick = []
    
    async def start(self):
        await super().start(
            primaryText="Verwalte die Teilnehmer.\nDu kannst Teilnehmer entfernen, indem du auf das ❌ unter der Nachricht klickst.",
            subText="Drück auf 👍, sobald du fertig bist.",
            reactions='👍'
        )

    async def sendNext(self):
        if not self.isValid():
            return
        
        msgs = []

        if self.pos == 0:
            async def sendUID(uid):
                msg = await self.user.send("<@" + str(uid) + ">")
                self.userMsgs[msg.id] = uid
                msgs.append(msg)
                self.eventLink.registerEvent(msg.id, self.onRemove)

            if self.infoDict["type"]:
                await asyncio.gather(
                    *[
                        sendUID(uid)
                        for uid in self.infoDict["entries"]
                    ]
                )
            else:
                uids = []
                for entries in self.infoDict["dates"].values():
                    for uid in entries:
                        if not uid in uids:
                            uids.append(uid)
                
                await asyncio.gather(
                    *[
                        sendUID(uid)
                        for uid in uids
                    ]
                )
            
            await asyncio.gather(
                *[
                    msg.add_reaction("❌")
                    for msg in msgs
                ]
            )
        
        self.lastMsg = msgs

    async def onRemove(self, wasSet, emoji, msg, user):
        if emoji != "❌":
            return
        
        uid = self.userMsgs[msg.id]

        try:
            if wasSet:
                self.willKick.append(uid)
            else:
                self.willKick.remove(uid)
        except:
            pass
    
    async def onHeaderReaction(self, wasSet, emoji, msg, user):
        if not await super().onHeaderReaction(wasSet, emoji, msg, user):
            if wasSet and emoji == '👍' and self.pos == 0:
                for msg in self.lastMsg:
                    self.eventLink.unregisterEvent(msg.id)
                
                self.kickedUsers = self.willKick

                if self.infoDict["type"]:
                    for uid in self.kickedUsers:
                        self.infoDict["entries"].remove(uid)
                else:
                    for uid in self.kickedUsers:
                        for entries in self.infoDict["dates"].values():
                            if uid in entries:
                                entries.remove(uid)

                self.done = True
                await self.close()
                
    def getDialogueType(self):
        return "KickDialogue"

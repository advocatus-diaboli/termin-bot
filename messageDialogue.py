from dialogue import Dialogue
from log import Log
import botUtil

import discord

class MessageDialogue(Dialogue):
    
    roundName = None
    message = None
    recips = []

    def __init__(self, eventLink, user, channel, infoDict):
        super().__init__(eventLink, user, channel)

        if infoDict["type"]:
            self.recips = infoDict["entries"]
        else:
            self.recips = []
            for parts in infoDict["dates"].values():
                for part in parts:
                    if not part in self.recips:
                        self.recips.append(part)
        
        self.roundName = infoDict["name"]

        self.lastMsg = []
    
    async def start(self):
        await super().start(
            primaryText="Benachrichtige die eingetragenen Teilnehmer:innen!",
            subText="Die Nachricht wird dann mit dem Rundennamen an alle Teilnehmer:innen geschickt, die sich für die Runde eingetragen haben."
        )

    
    async def sendNext(self):
        if not self.isValid():
            return
        if self.pos == 0:
            msg = await self.channel.send(
                None,
                embed=discord.Embed(
                    title="Gib die Nachricht zum verschicken ein:"
                )
            )
            self.lastMsg.append(msg)
            self.eventLink.registerEvent(self.channel.id, self.onMessage)

        elif self.pos == 1:
            msg = await self.user.send(
                None,
                embed=discord.Embed(
                    title="Die Nachricht wurde verschickt."
                )
            )
            self.lastMsg.append(msg)
            self.done = True
            await self.close()
    
    async def onMessage(self, msg):
        self.message = msg.content
        self.eventLink.unregisterEvent(self.channel.id)

        await botUtil.tryDelete(msg)

        self.nextPos()
        await self.sendNext()
    
    async def close(self):
        if self.done:
            self.lastMsg = None

        if(self.channel.id in self.eventLink):
            self.eventLink.unregisterEvent(self.channel.id)

        await super().close()
    
    def getDialogueType(self):
        return "MessageDialogue"

import dialogue

import botUtil

import discord

from asyncio import gather

from datetime import date, time, datetime
import dateTimeConv

import io

class EditDialogue(dialogue.Dialogue):

    infoDict = {}
    adminPostId = None

    dateMsgs = {}

    imgCache = None

    shouldNotify = False

    def __init__(self, eventLink, user, channel, infoDict, adminPostId, imgCache):
        super().__init__(eventLink, user, channel)

        self.infoDict = infoDict.copy()
        self.adminPostId = adminPostId
        self.dateMsgs = {}

        self.imgCache = imgCache

        self.shouldNotify = False
    
    async def start(self):
        await super().start(primaryText="Bearbeite deine Runde...")
    
    async def sendNext(self):
        if not self.isValid():
            return
        
        msg = None

        if self.pos == 0:
            msg = await self.channel.send(None, embed =
                botUtil.createBasicEmbed(
                    title="Was willst du bearbeiten?",
                    author=None,
                    fieldTitle="Optionen:",
                    fieldText=
                        "🖋️ Name\n" +
                        "📋 Beschreibung\n" +
                        "🔖 Anmerkungen\n"
                        "📖 System\n" +
                        "🖼️ Bild\n" +
                        "🔢 Max. Teilnehmer:innen\n" +
                        "🗓️ Termine\n\n" +
                        "Bei größeren Änderungen (z.B. an den Terminen) sollte, um Verwirrung zu vermeiden, ein neuer Post erstellt werden."
                )
            )

            await msg.add_reaction("🖋️")
            await msg.add_reaction("📋")
            await msg.add_reaction("🔖")
            await msg.add_reaction("📖")
            await msg.add_reaction("🖼️")
            await msg.add_reaction("🔢")
            await msg.add_reaction("🗓️")

            self.eventLink.registerEvent(msg.id, self.onChoiceReaction)
            self.eventBuffer.append(msg.id)

            self.lastMsg = msg
            return
            
        elif self.pos == 1:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib einen neuen Name für *" + self.infoDict["name"] + "* ein:"
                )
            )
        elif self.pos == 2:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib eine neue Beschreibung für *" + self.infoDict["name"] + "* an: (Max. 2000 Zeichen)"
                )
            )
        elif self.pos == 3:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib neue Anmerkungen für *" + self.infoDict["name"] + "* an: (Max. 1000 Zeichen)\n(Drück auf 🚫 zum Entfernen.)"
                )
            )
            await msg.add_reaction("🚫")
            self.eventLink.registerEvent(msg.id, self.onNoInfo)
            self.eventBuffer.append(msg.id)
        elif self.pos == 4:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib ein neues System für *" + self.infoDict["name"] + "* an: (Bisher: "
                         + self.infoDict["system"] + ")"
                )
            )
        elif self.pos == 5:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib ein neues Bild an:  (Bevorzugt als Link, sonst als Anhang) (Max. 1 MB)\n(Drück auf 🚫 zum Entfernen.)"
                )
            )
            await msg.add_reaction("🚫")
            self.eventLink.registerEvent(msg.id, self.onNoImg)
            self.eventBuffer.append(msg.id)
        elif self.pos == 6:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib eine neue max. Teilnehmer:innen-Anzahl für *" + self.infoDict["name"] + "* ein: (Bisher: "
                         + (str(self.infoDict["limit"]) if not self.infoDict["limit"] is None else "♾️") + ")\n(Drück auf ♾️, wenn du kein Teilnehmerlimit haben willst.)",
                )
            )
            await msg.add_reaction("♾️")
            self.eventLink.registerEvent(msg.id, self.onSetLimitInfinity)
            self.eventBuffer.append(msg.id)
        elif self.pos == 7:
            
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Gib die neuen Termine für *" + self.infoDict["name"] + "* ein: (Max. 21)\n(Drück auf 🗑️, um alte Termine zu entfernen, 👍, wenn du fertig bist.)"
                )
            )
            self.eventLink.registerEvent(msg.id, self.onDateEntryDone)

            iter = 0

            for date in self.infoDict["dates"]:
                iter += 1
                tmp = await self.channel.send(
                    str(iter) + ')  ' + dateTimeConv.dateTimeToStr(datetime.fromtimestamp(float(date)))
                )
                await tmp.add_reaction("🗑️")

                self.dateMsgs[tmp.id] = date

                self.eventLink.registerEvent(tmp.id, self.onDateDelete)
                self.eventBuffer.append(msg.id)

            await msg.add_reaction("👍")
        elif self.pos == 32:
            msg = await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Soll der Post neu verschickt werden, damit alle über die Änderungen benachrichtigt werden?"
                )
            )
            await msg.add_reaction("✅")
            await msg.add_reaction("❌")
            self.eventLink.registerEvent(msg.id, self.onNotifyChoice)
            self.eventBuffer.append(msg.id)
            
            self.lastMsg = msg
            return
        else:
            return
        
        self.lastMsg = [msg]

        self.eventLink.registerEvent(self.channel.id, self.onMessage)
        self.eventBuffer.append(self.channel.id)
    
    async def onChoiceReaction(self, wasSet, emoji, msg, user):
        if not wasSet:
            return
        
        if emoji == "🖋️":
            self.pos = 1
        elif emoji == "📋":
            self.pos = 2
        elif emoji == "🔖":
            self.pos = 3
        elif emoji == "📖":
            self.pos = 4
        elif emoji == "🖼️":
            self.pos = 5
        elif emoji == "🔢":
            self.pos = 6
        elif emoji == "🗓️":
            self.pos = 7
        else:
            return
        
        self.clearEventBuffer()
        await botUtil.tryDelete(self.lastMsg)

        await self.sendNext()
    
    async def onMessage(self, msg):
        if self.pos == 1:
            self.infoDict["name"] = msg.content
        elif self.pos == 2:
            if len(msg.content) > 2000:
                tmp = await self.channel.send("*Die Beschreibung ist mit " + str(len(msg.content)) + " Zeichen leider zu lang*")
                await tmp.add_reaction("🔥")
                return
            self.infoDict["desc"] = msg.content
        elif self.pos == 3:
            if len(msg.content) > 1000:
                tmp = await self.channel.send("*Die Anmerkung ist mit " + str(len(msg.content)) + " Zeichen leider zu lang*")
                await tmp.add_reaction("🔥")
                return
            self.infoDict["info"] = msg.content
        elif self.pos == 4:
            self.infoDict["system"] = msg.content
        elif self.pos == 5:
            if msg.content == "":
                if len(msg.attachments) == 0:
                    tmp = await self.channel.send("*Ich habe leider kein Bild in deiner Nachricht gefunden*")
                    await tmp.add_reaction("🔥")
                    return
                else:
                    if msg.attachments[0].size > 0xfffff:
                        tmp = await self.channel.send("*Der Anhang ist zu groß, das Bild darf maximal **1 MB** groß sein.*")
                        await tmp.add_reaction("🔥")
                        return
                    else:
                        if self.imgCache is None:
                            tmp = await self.channel.send("*Ich kann auf diesem Server im Moment keine Bilder zwischenspeichern, bitte schick mir ein Bild als Link*")
                            self.lastMsg.append(tmp)
                            self.lastMsg.append(msg)
                            return

                        imgData = await msg.attachments[0].read()
                        bs = io.BytesIO(imgData)
                        imgFile = discord.File(bs, filename=msg.attachments[0].filename)

                        tmp = await self.imgCache.send("Bild für: **" + self.infoDict["name"] + "**", file=imgFile)
                        self.infoDict["img"] = tmp.attachments[0].url
            else:
                self.infoDict["img"] = msg.content
        elif self.pos == 6:
            if msg.content.lower() == "♾️":
                self.infoDict["limit"] = None
            try:
                tmp = int(msg.content)
                if tmp <= 0:
                    raise ValueError
                self.infoDict["limit"] = tmp
            except ValueError:
                #await botUtil.tryDelete(msg)      #Cannot succed as the dialogue channel is always a DM Channel
                tmp = await self.channel.send("*Bitte gib eine valide Anzahl ein*")
                await tmp.add_reaction("🔥")
                return
        elif self.pos == 7:
            dateEntries = msg.content.split('\n')
            errors = []
            invalids = []

            tday = datetime.now()

            if len(dateEntries) + len(self.infoDict["dates"]) > 21:
                tmp = await self.channel.send(
                    "*Das sind zu viele Termine*" + 
                    ("\nDu kannst noch maximal " + str(21 - len(self.infoDict["dates"])) + "angeben." if len(self.infoDict["dates"]) < 21 else "")
                )
                await tmp.add_reaction("🔥")
                return

            for entry in dateEntries:
                entry = entry.strip()
                
                if len(entry) == 0:
                    continue

                dt = dateTimeConv.strToDateTime(entry)

                if dt is None:
                    errors.append(entry)
                    continue

                if dt < tday and dt.time() != time(0, 0, 0, 0):
                    invalids.append(entry)
                    continue

                stamp = dt.timestamp()

                if not stamp in self.infoDict["dates"]:
                    if self.infoDict["type"]:
                        self.infoDict["dates"].append(stamp)
                    else:
                        self.infoDict["dates"][stamp] = []
            
            if len(errors) > 0:
                tmp = None
                if len(errors) == 1:
                    tmp = await self.channel.send("*Den Termin '" + errors[0] + "' hab ich leider nicht verstanden*")
                else:
                    tmp = await self.channel.send("*Die folgenden Termine habe ich nicht verstanden:*\n" + '\n'.join(errors))
                await tmp.add_reaction("🔥")

            if len(invalids) > 0:
                tmp = None
                if len(invalids) == 1:
                    tmp = await self.channel.send("*Der Termin '" + invalids[0] + "' liegt in der Vergangenheit*")
                else:
                    tmp = await self.channel.send("*Die folgenden Termine liegen in der Vergangenheit:*\n" + '\n'.join(invalids))
                await tmp.add_reaction("🔥")
            
            return
        else:
            return

        self.clearEventBuffer()

        await self.channel.send(
            None,
            embed=botUtil.createBasicEmbed(
                title="Die Änderung wurde gespeichert."
            )
        )

        self.pos = 32
        await self.sendNext()
    
    async def onNoImg(self, wasSet, emoji, msg, user):
        if wasSet and emoji == "🚫":
            self.clearEventBuffer()
            
            await self.channel.send("🚫")
            self.infoDict["img"] = None

            await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Die Änderung wurde gespeichert."
                )
            )

            self.pos = 32
            await self.sendNext()

    async def onNoInfo(self, wasSet, emoji, msg, user):
        if wasSet and emoji == "🚫":
            self.clearEventBuffer()
            
            await self.channel.send("🚫")
            self.infoDict["info"] = None

            await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Die Änderung wurde gespeichert."
                )
            )

            self.pos = 32
            await self.sendNext()

    async def onSetLimitInfinity(self, wasSet, emoji, msg, user):
        if wasSet and emoji == "♾️":
            self.clearEventBuffer()

            #await msg.clear_reaction("♾️")    # Cannot succed as the dialogue channel is always a DM Channel
            await self.channel.send("♾️")
            self.infoDict["limit"] = None
            
            await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Die Änderung wurde gespeichert."
                )
            )

            self.pos = 32
            await self.sendNext()

    async def onDateEntryDone(self, wasSet, emoji, msg, user):
        if wasSet and emoji == "👍":
            self.clearEventBuffer()
            self.eventLink.unregisterEvent(msg.id)
            
            await self.channel.send(
                None,
                embed=botUtil.createBasicEmbed(
                    title="Die Änderung wurde gespeichert."
                )
            )
            
            if self.infoDict["type"]:
                self.infoDict["dates"].sort()
            else:
                self.infoDict["dates"] = dict(sorted(self.infoDict["dates"].items(), key=lambda item: float(item[0])))

            self.pos = 32
            await self.sendNext()
    
    async def onDateDelete(self, wasSet, emoji, msg, user):
        if wasSet and emoji == "🗑️":
            self.eventLink.unregisterEvent(msg.id)
            
            if self.infoDict["type"]:
                self.infoDict["dates"].remove(self.dateMsgs[msg.id])
            else:
                self.infoDict["dates"].pop(self.dateMsgs[msg.id])

            await botUtil.tryDelete(msg)
        
    async def onNotifyChoice(self, wasSet, emoji, msg, user):
        if wasSet and emoji in ["✅", "❌"]:
            self.clearEventBuffer()

            if emoji == "✅":
                self.shouldNotify = True
            
            self.done = True
            await self.close()

    async def close(self):
        self.clearEventBuffer()
        await super().close()
    
    def getDialogueType(self):
        return "EditDialogue"



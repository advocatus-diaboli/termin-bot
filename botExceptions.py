class PostCreationFailedException(Exception):
    pass

class PostUpdateFailedException(Exception):
    pass
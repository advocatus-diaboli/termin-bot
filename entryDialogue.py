import asyncio
import discord

from dialogue import Dialogue

import botUtil

import datetime
import dateTimeConv

from log import Log

class EntryDialogue(Dialogue):
    dates = {}
    infoDict = None
    msgId = None

    dateMsgs = {}

    def __init__(self, eventLink, user, channel, infoDict, msgId):
        super().__init__(eventLink, user, channel)
        self.infoDict = infoDict
        self.msgId = msgId
        self.dates = {}
        self.dateMsgs = {}

    async def start(self):
        await super().start("Bitte wähl die Termine aus, an denen du kannst.",
            subText=
            "\nDrück auf '👍', wenn du kannst." +
            "\nWenn du fertig bist drück auf das 🆗, sobald du fertig bist." +
            "\nFalls du dich schonmal eingetragen hast, werden deine alten Angaben überschrieben.", 
            reactions="🆗"
        )
    
    async def onHeaderReaction(self, wasSet, emoji, msg, user):
        if await super().onHeaderReaction(wasSet, emoji, msg, user):
            return
        elif emoji == "🆗" and wasSet:
            self.done = True
            await self.close()
            return
    
    async def sendNext(self):
        if not self.isValid():
            raise ValueError("The dialogue is invalid.")
        msgs = []
        if self.pos == 0:
            for date in self.infoDict["dates"]:

                dt = datetime.datetime.fromtimestamp(float(date))

                tmp = await self.channel.send(dateTimeConv.dateTimeToStr(dt))
                self.dates[date] = False

                self.eventLink.registerEvent(tmp.id, self.onDateReaction)

                msgs.append(tmp)

                self.dateMsgs[tmp.id] = date
            
            res = await asyncio.gather(
                *[
                    msg.add_reaction('👍')
                    for msg in msgs
                ],
                return_exceptions=True
            )

            iter = 0
            for elem in res:
                if isinstance(elem, Exception):
                    Log.warning("Failed to add a reaction to a message, trying again")
                    try:
                        await msgs[iter].add_reaction('👍')
                    except Exception as ex:
                        Log.exception(ex)
                iter += 1


        self.lastMsg = msgs
        self.nextPos()
    
    async def onDateReaction(self, wasSet, emoji, msg, user):
        if emoji == '👍':
            self.dates[self.dateMsgs[msg.id]] = wasSet

    async def close(self):
        if not self.lastMsg is None:
            for msg in self.lastMsg:
                self.eventLink.unregisterEvent(msg.id)
        await super().close()
    
    def getDialogueType(self):
        return "EntryDialogue"



import datetime
import os

import traceback

class Log:
    logFile = None
    instanceCount = 0
    lines = 0

    def __init__(self) -> None:
        pass
    
    def __enter__(self):
        Log.instanceCount += 1
        Log.open()
        return self
    
    def __exit__(self):
        Log.instanceCount -= 1

        if Log.instanceCount == 0 and not Log.logFile.closed:
            Log.logFile.close()
        
        return self
    
    def open():
        if not Log.isOpen():
            fileName = "data/Logs/" + datetime.datetime.now().strftime("%Y-%m-%d/%H-%M-%S") + ".log"
            os.makedirs(os.path.dirname(fileName), exist_ok=True)
            Log.logFile = open(fileName, "w", encoding='utf8')

    def isOpen() -> bool:
        return Log.logFile is not None and not Log.logFile.closed

    def close():
        if not Log.isOpen():
            return
        try:
            Log.logFile.close()
        except Exception as ex1:
            try:
                Log.error("Tried to close this file and failed.")
            except Exception as ex2:
                try:
                    print("\n\nERROR!\n\nLog closing failed!\n\t" + str(ex1) + "\n\nLogging of failed Log closing also failed.\n\t" + str(ex2) + "\n\nERROR!\n\n")
                except:
                    pass

    def __iadd__(*text):
        Log.status(*text)
    
    def debug(*text):
        Log.log('DEBUG__', text)
        Log.flush()

    def status(*text):
        Log.log('STATUS_', *text)
        Log.flush()

    def warning(*text):
        Log.log('WARNING', *text)
        Log.flush()

    def error(*text):
        Log.log('ERROR__', *text)
        Log.flush()

    def exception(*ex):
        try:
            Log.log('!EXCEPT', *ex, '\n\n', traceback.format_exc())
        except:
            Log.log('!EXCEPT', *ex)
        Log.flush()
    
    def log(type, *text):
        if not Log.isOpen():
            return
        try:
            Log.raw('[', type, '] ', datetime.datetime.now(), ':\t', ''.join([str(t) for t in text]), '\n')
            Log.lines += 1
        
        except Exception as ex:
            try:
                print("\n\nERROR!\n\nLogging failed!\n\t" + str(ex) + "\n\nERROR!\n\n")
            except:
                pass
    
    def raw(*content):
        if not Log.isOpen():
            return
        try:
            Log.logFile.write(''.join([str(c) for c in content]))
            Log.lines += 1
        
        except Exception as ex:
            try:
                print("\n\nERROR!\n\nLogging failed!\n\t" + str(ex) + "\n\nERROR!\n\n")
            except:
                pass
    
    def emptyLine():
        if not Log.isOpen():
            return
        try:
            if not Log.logFile.closed:
                Log.raw('\n')
                Log.lines += 1
        
        except Exception as ex:
            try:
                print("\n\nERROR!\n\nLogging failed!\n\t" + str(ex) + "\n\nERROR!\n\n")
            except:
                pass

    def flush():
        if not Log.isOpen():
            return
        try:
            Log.logFile.flush()
            Log.lines = 0
        except Exception as ex:
            try:
                print("\n\nERROR!\n\nLog flushing failed!\n\t" + str(ex) + "\n\nERROR!\n\n")
            except:
                pass
